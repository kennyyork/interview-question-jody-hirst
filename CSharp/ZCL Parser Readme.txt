
Created by: Jody Hirst for Interview with CentraLite

Functionality:

1. Open Button
   The open button reads a .zcl file into a StreamReader.  The StreamReader is read line by line in a loop.  A ZCL Reader class was created to handle the parsing of each line passed to it into individual fields.  This class may be a little heavy on the use of statics, but this was a quick design and implementation idea to forgo the need to instantiate a type.  This class acts more as a helper utility.  The data is displayed in a C# listview object on the main form.

2. Save Button
   The save button allows the user to save the parsed contents of the .zcl file to a tab delineated .txt file for later use.  The column headers are saved to the first line of the file.

3. Help Button
   This button opens an instance of Adobe Reader, and displays the format specifications that this tool was written against for future reference.   