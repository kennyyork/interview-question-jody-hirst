﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;


namespace CentraLite_ZCL_Reader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = @"Ready";

            //add cols to listview with appropriate headers
            listZCLView.Columns.Add("Cluster", 45);
            listZCLView.Columns.Add("Frame Type", 73);
            listZCLView.Columns.Add("MFG Code?", 70);
            listZCLView.Columns.Add("MFG Code", 110);
            listZCLView.Columns.Add("Direction", 75);
            listZCLView.Columns.Add("Def. Response", 100);
            listZCLView.Columns.Add("Transaction", 70);
            listZCLView.Columns.Add("Command", 210);
            listZCLView.Columns.Add("Payload", 600);

            listZCLView.View = View.Details;
            listZCLView.GridLines = true;
            listZCLView.FullRowSelect = true;
            listZCLView.Columns[listZCLView.Columns.Count - 1].Width = -2;
        }

        /// <summary>
        /// Handler for Open File button on toolbar.
        /// Reads data from file into stream.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = "c:\\",
                Filter = @"zcl files (*.zcl)|*.zcl",
                FilterIndex = 2,
                RestoreDirectory = true
            };
            
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Stream inStream = openFileDialog1.OpenFile();
                    ReadData(new StreamReader(inStream));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(@"Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Handler for Save button click on toolbar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog
            {
                Title = @"SaveFileDialog Export2File",
                Filter = @"Text File (.txt) | *.txt"
            };

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string filename = sfd.FileName;
                if (filename != "")
                {
                    using (StreamWriter sw = new StreamWriter(filename))
                    {
                        //export the Column Header names to the file first
                        var sb = new StringBuilder();
                        foreach (ColumnHeader item in listZCLView.Columns)
                        {
                            sb.Append(string.Format("{0}\t", item.Text));
                        }
                        sw.WriteLine(sb.ToString());

                        //iterate over each column
                        foreach (ListViewItem item in listZCLView.Items)
                        {
                            sb = new StringBuilder();
                            
                            //iterate over each row's cells
                            foreach (ListViewItem.ListViewSubItem listViewSubItem in item.SubItems)
                            {
                                sb.Append(string.Format("{0}\t", listViewSubItem.Text));
                            }
                            sw.WriteLine(sb.ToString());
                        }
                        sw.Close();
                   }
                }
            }
       }

        /// <summary>
        /// Handler for Help button click on toolbar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void helpToolStripButton_Click(object sender, EventArgs e)
        {
            Process.Start("Resources\\Zigbee Command Frame Formats.pdf");
        }

        /// <summary>
        /// Read file in StreamReader and output to listview
        /// </summary>
        /// <param name="zclFileIn"></param>
        private void ReadData(StreamReader zclFileIn)
        {
            String line;
            int count = 0;
            listZCLView.BeginUpdate();//pause listview refreshing - diminishes flickering

            //read each line and parse into form's listview
            while ((line = zclFileIn.ReadLine()) != null)
            {
                ZclReader.ParseZigbeePacket(line);
                listZCLView.Items.Add(new ListViewItem(ZclReader.ZigCmdLine));
                ++count;
            }
            toolStripStatusLabel1.Text = String.Format(@"Processing Complete - {0} Clusters Processed ",count);
            AutoResizeColumns(listZCLView);//helper function
            listZCLView.EndUpdate();//reenable refreshing listview
        }

        /// <summary>
        /// Resize cols to fit header text or col data.  Header text may be wider than widest data in col
        /// </summary>
        /// <param name="lv"></param>
        public static void AutoResizeColumns(ListView lv)
        {
            lv.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            ListView.ColumnHeaderCollection cc = lv.Columns;
            for (int i = 0; i < cc.Count; i++)
            {
                int colWidth = TextRenderer.MeasureText(cc[i].Text, lv.Font).Width + 10;
                if (colWidth > cc[i].Width)  //is header text wider than column width?
                {
                    cc[i].Width = colWidth;
                }
            }
        }
    }
}
