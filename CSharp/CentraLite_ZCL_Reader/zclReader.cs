﻿using System;
using System.Collections.Generic;


namespace CentraLite_ZCL_Reader
{
	/// <summary>
	/// Utility class to encapsulate ZCL file reading
	/// </summary>
	public class ZclReader
    {
        #region public 

        //properties for parsed text from packet
		public static string Cluster    { get; set; }
		public static string FrameType  { get; set; }
		public static string IsMfgCode  { get; set; }
		public static string Direction  { get; set; }
		public static string Response   { get; set; }
		public static string MfgCode    { get; set; }
		public static string Trans      { get; set; }
		public static string Command    { get; set; }
		public static string Payload    { get; set; }

		public static string[] ZigCmdLine = new string[9];

        #endregion

        #region Private fields

        /// <summary>
		/// Dictionary containing the mappings between bit values and Zigbee command meaning
		/// </summary>
		private static readonly Dictionary<string, string> ZclCommandDict = new Dictionary<string, string>
																	{
																		{"00", "Read Attributes"},
																		{"01", "Read Attributes Responses"},
																		{"02", "Write Attributes"},
																		{"03", "Write Attributes Undivided"},
																		{"04", "Write Attributes Response"},
																		{"05", "Write Attributes No Response"},
																		{"06", "Configure Reporting"},
																		{"07", "Configure Reporting Response"},
																		{"08", "Read Reporting Configuration"},
																		{"09", "Read Reporting Configuration Response"},
																		{"0A", "Report Attributes"},
																		{"0B", "Default Response"},
																		{"0C", "Discover Attributes"},
																		{"0D", "Discover Attributes Response"},
																		{"0E", "Read Attributes Structured"},
																		{"0F", "Write Attributes Structured"},
																		{"10", "Write Attributes Structured Response"},
																		{"99", "None Applicable"}
																	};

		private static readonly Dictionary<string, string> ZclDirectionDict = new Dictionary<string, string>
																	{
																		{"0","From Client"},
																		{"1","From Server"}
																	};

		private static readonly Dictionary<string, string> ZclFrameTypeDict = new Dictionary<string, string>
																	{
																		{"00","Entire Profile"},
																		{"01","Cluster"},
																		{"10","Reserved"},
																		{"11","Reserved"}
																	};

        private static readonly Dictionary<string, string> ZclDefResponseDict = new Dictionary<string, string>
																	{
																		{"0","On"},
																		{"1","Off"}
																	};

        //classes to hold length and string position of cmd fields
		private static readonly ZclField MfgFrType = new ZclField(6,2);

		private static readonly ZclField FrameCtrl = new ZclField(0,2);

		private static readonly ZclField MfgDir = new ZclField(4,1);

		private static readonly ZclField MfgCodePresent = new ZclField(5,1);

		private static readonly ZclField DefResp = new ZclField(3,1);

		private static int _counter;//count of clusters processed.

        #endregion

        #region Methods
        
        /// <summary>
		/// Parse input string into individual elements. Position w/in string
		/// is based on Zigbee Command Frame Format
		/// </summary>
		/// <param name="line"></param>
		public static void ParseZigbeePacket(string line)
		{
			if (line != null)
			{
				//Count each line as a cluster
				_counter += 1;
				Cluster = Convert.ToString(_counter);

			   //Frame Control - First 8 bits.  Convert to binary
				string frameControl = line.Substring(FrameCtrl.StartIndex, FrameCtrl.Len);
				var convertedBinary = Convert.ToString(Convert.ToInt32(frameControl, 16), 2).PadLeft(8, '0');

				//Determine Manufactor Frame Type subfield from binary - 2 bits in length
				String frType = convertedBinary.Substring(MfgFrType.StartIndex, MfgFrType.Len);
				FrameType = ZclFrameTypeDict.ContainsKey(frType) ? ZclFrameTypeDict[frType] : "Error";

			   //Direction of packet -> client/server direction for this command
				String dir = convertedBinary.Substring(MfgDir.StartIndex, MfgDir.Len);
                Direction = ZclDirectionDict.ContainsKey(dir) ? ZclDirectionDict[dir] : "Error"; 

				//Manufacturer Specific Sub Field - specifies whether cmd refers to mfg specific extension to profile.
				string isManufacturerCodeEnabled = convertedBinary.Substring(MfgCodePresent.StartIndex, MfgCodePresent.Len);
				IsMfgCode = Convert.ToInt32(isManufacturerCodeEnabled) == 0 ? "No" : "Yes ";

				//Default Response - disable default response subfield is 1 bit in length
			    string defResp = convertedBinary.Substring(DefResp.StartIndex, DefResp.Len);
			    Response = ZclDefResponseDict.ContainsKey(defResp) ? ZclDefResponseDict[defResp] : "Error";

				//Manufacturer Code Present? Section
				string commandIdentity;
				string mCode;

				if (Convert.ToInt32(isManufacturerCodeEnabled) == 1)
				{
					mCode = line.Substring(2, 4);
					convertedBinary = Convert.ToString(Convert.ToInt32(mCode, 16), 2).PadLeft(16, '0');
					MfgCode = convertedBinary;

					Trans = line.Substring(6, 2);           //Transaction Sequence Number
					commandIdentity = line.Substring(8, 2); //Command Identifier

					//Extract long payload from remainder of string
					Payload = line.Length > 10 ? line.Remove(0, 9) : "None";
				}
				else
				{
					MfgCode = "";
					Trans = line.Substring(2, 2);           //Transaction Sequence Number
					commandIdentity = line.Substring(4, 2); //Command Identifier

					//Extract short payload from remainder of line
					Payload = line.Length > 6 ? line.Remove(0, 5) : "None";
				}

				//retrieve Zigbee cmd from dict based on bit values
				Command = ZclCommandDict.ContainsKey(commandIdentity) ? ZclCommandDict[commandIdentity] : "None Applicable";

				//assign individual values to array for use as a cmd string later
				ZigCmdLine[0] = Cluster;
				ZigCmdLine[1] = FrameType;
				ZigCmdLine[2] = IsMfgCode;
				ZigCmdLine[3] = MfgCode;
				ZigCmdLine[4] = Direction;
				ZigCmdLine[5] = Response;
				ZigCmdLine[6] = Trans;
				ZigCmdLine[7] = Command;
				ZigCmdLine[8] = Payload;
			}
		}//end parsezigbeepacket method

        #endregion
    }//end zclreader class
}

/// <summary>
/// Class to store the location and length of bit positions in zcl cmd string.
/// </summary>
internal class ZclField
{
	public int StartIndex;
	public int Len;

	//ctor
	public ZclField(int startIndex, int len)
	{
		StartIndex = startIndex;
		Len = len;
	}
};